package com.petstore

class Product {

  static belongsTo = [category: Category]

  String sku
  String name
  String description
  Float price

  static constraints = {
    sku (blanks: false, unique: true)
    name (blanks: false, unique: true)
    category (nullable: true)
    description (nullable: true)
    price (nullable: false)
  }

  String toString() {
    return name
  }
}