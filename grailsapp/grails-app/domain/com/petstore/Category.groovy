package com.petstore

class Category {

  static belongsTo = [parent: Category]

  String code
  String name

  static constraints = {
    code (blanks: false, unique: true)
    name (blanks: false, unique: true)
    parent (nullable: true)
  }

  String toString() {
    return name
  }
}
