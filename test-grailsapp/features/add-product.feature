Feature: Add a Product
  As a Site Administrator
  I want to add new Products
  In order to sell them to Customers

  Background:
    Given a Dog's Food Category

  Scenario: Add a new Product
    Given I am on the page to add Products
    When I enter the Product:
      | sku         | B005DFLFKG                                  |
      | name        | Purina Dog Chow                             |
      | category    | Dog Food                                    |
      | description | Recommended Daily Feeding Amounts for Dogs  |
      | price       | 28.0                                        |
    Then I should see a succesful message ""

    Scenario: Avoid add Products with same SKU
      Given same Product previously added
        And I am on the page to add Products
      When I enter the Product:
        | sku         | B005DFLFKH                                  |
        | name        | Purina Dog Chow                             |
        | category    | Dog Food                                    |
        | description | Recommended Daily Feeding Amounts for Dogs  |
        | price       | 28.0                                        |
      Then I should see an error message ""
    
    
    