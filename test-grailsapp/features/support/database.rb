require 'active_record'

ActiveRecord::Base.establish_connection(  
  :adapter => "mysql",  
  :host => "172.17.0.2",
  :username => "admin",
  :password => "toor",
  :database => "grailsapp-dev"
)

class Category < ActiveRecord::Base
  self.table_name = "category"

  belongs_to :category, foreign_key: "parent_id"
end

class Product < ActiveRecord::Base
  self.table_name = "product"

  belongs_to :category
end 