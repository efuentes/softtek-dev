Feature: Add a Category
  As a Site Administrator
  I want to add new Categories
  In order to group Products so Customer can find them easily

  Scenario: Add a parent Category
    Given I am on the page to add Categories
    When I enter "Dogs" in Category's "Name"
      And I enter "dogs" in Category's "Code"
      And I click on button "Create"
    Then I should see a message "Show ProductCategory"

  Scenario: Add a child Category
    Given an existing "Dogs" category
      And I am on the page to add Categories
    When I enter "Dogs Food" in Category's "Name"
      And I enter "dogs_food" in Category's "Code"
      And I select "Dogs" from parent's Category code
      And I click on button "Create"
    Then I should see a message "Show ProductCategory"

  Scenario: Avoid adding an existing Category's code
    Given an existing "Dogs" category
      And I am on the page to add Categories
    When I enter "Puppy Dogs" in Category's "Name"
      And I enter "dogs" in Category's "Code"
      And I click on button "Create"
    Then I should see an error "Category's code already exists."

  Scenario: Avoid adding an existing Category's name
    Given an existing "Dogs" category
      And I am on the page to add Categories
    When I enter "Dogs" in Category's "Name"
      And I enter "doggys" in Category's "Code"
      And I click on button "Create"
    Then I should see an error "Category's name already exists."

  Scenario: Avoid adding a Category without a name
    Given I am on the page to add Categories
    When I enter "" in Category's "Name"
      And I enter "fishes" in Category's "Code"
      And I click on button "Create"
    Then I should see a popup error "Please fill out this field."

  Scenario: Avoid adding a Category without a code
    Given I am on the page to add Categories
    When I enter "Fishes" in Category's "Name"
      And I enter "" in Category's "Code"
      And I click on button "Create"
    Then I should see a popup error "Please fill out this field."