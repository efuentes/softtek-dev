Given(/^a Dog's Food Category$/) do
  category = create(:dog_food_category)
end

Given(/^I am on the page to add Products$/) do
  visit '/product/create'
end

Given(/^same Product previously added$/) do
  product = create(:dog_food_product)
end

When(/^I enter the Product:$/) do |table|
  table.rows_hash.each do |field, value|
    if field === 'category'
      select value, :from => field
    else
      fill_in field, :with => value
    end
  end
  click_on("Create")
end

Then(/^I should see a succesful message "(.*?)"$/) do |msg|
  expect(page).to have_css('div.message')
end

Then(/^I should see an error message "(.*?)"$/) do |err|
  expect(page).to have_css('ul.errors')
end

