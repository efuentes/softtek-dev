Given(/^I am on the page to add Categories$/) do
  visit '/category/create'
end

When(/^I enter "(.*?)" in Category's "(.*?)"$/) do |value, field|
  fill_in(field, :with => value)
end

When(/^I click on button "(.*?)"$/) do |button|
  click_on(button)
end

Given(/^an existing "(.*?)" category$/) do |category|
  #category = Category.create(code: "dogs", name: category, version: 0)
  category = create(:dog_category)
end

Given(/^a "(.*?)" Category's Code previously added$/) do |category|
  pending # express the regexp above with the code you wish you had
end

When(/^I select "(.*?)" from parent's Category code$/) do |parent_category|
  select(parent_category, :from => 'Parent')
end

Then(/^I should see a message "(.*?)"$/) do |msg|
  expect(page).to have_css('div.message')
  #expect(page).to have_content(msg)
end

Then(/^I should see an error "(.*?)"$/) do |err|
  expect(page).to have_css('ul.errors')
end

Then(/^I should see a popup error "(.*?)"$/) do |err|
  #expect(page).to have_xpath("//input[@required='required']")
  pending
end